package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.sound.sampled.SourceDataLine;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean newRound = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true) {
            if(newRound) {
                System.out.println("Let's play round " + roundCounter);
            }
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?");
            if(!humanMove.equals("rock") && !humanMove.equals("paper") && !humanMove.equals("scissors")) {
                    
				System.out.println("I do not understand " + humanMove + " Could you try again?");
                newRound = false;
			} else {
                newRound = true;
                
				int rand = (int)(Math.random()*3);
				
				
				String computerMove = "";
				if(rand == 0) {
					computerMove = "rock";
				} else if(rand == 1) {
					computerMove = "paper";
			    } else {
				    computerMove = "scissors";
				}
				
				    //Print the results of the game: tie, lose, win
				    if(humanMove.equals(computerMove)) {
					    System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". It's a tie!");
				    } else if((humanMove.equals("rock") && computerMove.equals("scissors")) || (humanMove.equals("scissors") && computerMove.equals("paper")) || (humanMove.equals("paper") && computerMove.equals("rock"))) {
					    System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Human wins!");
                     humanScore++;
				    } else {
					    System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Computer wins!");
                        computerScore++;
				    }
                    //Increment the round counter
                    roundCounter++;
                    // Print the standing score
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    // Prompt quit or continue playing
                    String quitGame = readInput("Do you wish to continue playing? (y/n)?");
                    if(quitGame.equals("n")) {
                    
                        System.out.println("Bye bye :)");
                        break;
                    } else {
                        continue;
                    }
			    }
                
            
		    }

		

    }

        
        // TODO: Implement Rock Paper Scissors
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
